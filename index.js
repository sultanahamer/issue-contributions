import fs from 'fs';
import { Gitlab } from '@gitbeaker/node';
import { getExistingContributors } from "./contributorsrc_reader.js";
const token = 'blah_blah';

const api = new Gitlab({
  token 
})
const projectId = '8972123';
const contributorIds = new Set();
const existingContributorProfileUrls = new Set();
const contributors = [];

const addContributor = (contributor) => {
  if (existingContributorProfileUrls.has(contributor.web_url) || contributorIds.has(contributor.username)) return;
  contributors.push(contributor);
  contributorIds.add(contributor.username);
}


const processNextSetOfIssueDiscussions = (issueIId, projectId, paginationInfo) => {
  return fetchNextSetOfIssueDiscussions(issueIId, projectId, paginationInfo)
    .then(({data: issueDiscussions, paginationInfo}) => {
      const allAsyncWork = [];
      if (paginationInfo.next != null) allAsyncWork.push(processNextSetOfIssueDiscussions(issueIId, projectId, paginationInfo));
      allAsyncWork.push(issueDiscussions.map(processIssueDiscussion));
      return Promise.all(allAsyncWork);
    });
}

const processIssueDiscussion = (issueDiscussion) => {
  return issueDiscussion.notes.map(note => addContributor(note.author));
}

const fetchNextSetOfIssueDiscussions = (issueIId, projectId, paginationInfo) => {
  const nextPage = (paginationInfo && paginationInfo.next) || 1;
  console.log('fetching issueDiscussions for issue', issueIId, 'and page', nextPage);
  return api.IssueDiscussions.all(projectId, issueIId, { perPage: 100, page: nextPage, showExpanded: true});
};

const processIssue = async (issue) => {
  addContributor(issue.author);
  return processNextSetOfIssueDiscussions(issue.iid, projectId);
}

const processNextSetOfIssues = (projectId, paginationInfo) => {
  return fetchNextSetOfIssues(projectId, paginationInfo)
    .then(({data: issues, paginationInfo}) => {
      const allAsyncWork = [];
      if (paginationInfo.next != null) allAsyncWork.push(processNextSetOfIssues(projectId, paginationInfo));
      allAsyncWork.push(Promise.all(issues.map(processIssue)));
      return Promise.all(allAsyncWork);
    });
}

const fetchNextSetOfIssues = (projectId, paginationInfo) => {
  const nextPage = (paginationInfo && paginationInfo.next) || 1;
  console.log('fetching issues page', nextPage);
  return api.Issues.all({projectId , perPage: 100, page: nextPage, showExpanded: true})
}

const existingContributorsFromFile = getExistingContributors("./.all-contributorsrc");
existingContributorsFromFile.forEach(contributor => {
  existingContributorProfileUrls.add(contributor.profile);
});

console.log('existing contributors are', existingContributorProfileUrls.size);
console.log('fetching all issues, will take good amount of time...');
await processNextSetOfIssues(projectId);
console.log('all async should be done');
contributorIds.forEach(contributorId => console.log('npx all-contributors add', contributorId, 'ideas,bug'));

const existingNames = existingContributorsFromFile.map(contributor => `<item>${contributor.name}</item>`);
const existingUrls = existingContributorsFromFile.map(contributor => `<item>${contributor.profile}</item>`);
const names = contributors.map(contributor => `<item>${contributor.name}</item>`);
const urls = contributors.map(contributor => `<item>${contributor.web_url}</item>`);
const prepareContentInNewLines = contentArray => contentArray.reduce((content, currentContent) => content +"\n" + currentContent, '');
const androidContributorsResourceContent = `<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string-array name="names">
    ${prepareContentInNewLines(existingNames)}
    ${prepareContentInNewLines(names)}
    </string-array>
    <string-array name="urls">
    ${prepareContentInNewLines(existingUrls)}
    ${prepareContentInNewLines(urls)}
    </string-array>
</resources>`;
fs.writeFile('./contributor.xml', androidContributorsResourceContent, err => {
  if (err) {
    console.error(err)
    return
  }
  console.log('android contributors resource file written successfully');
})
