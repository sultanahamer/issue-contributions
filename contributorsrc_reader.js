import fs from "fs";
export const getExistingContributors = rcFilePath => {
  try {
    const fileData = fs.readFileSync(rcFilePath, { encoding: 'utf-8', flag: 'r' });
    const contributorsrc = JSON.parse(fileData);
    return contributorsrc.contributors || [];
  }
  catch (err) {
    console.error('Error while reading exisiting contributors', err);
    throw err;
  }
};
