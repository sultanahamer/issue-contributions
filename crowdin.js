import { Users } from '@crowdin/crowdin-api-client';
import { getExistingContributors } from './contributorsrc_reader.js'
import { write as writeAndroidContributorsResource } from './generate-android-res.js';

const projectId = 383345
const token = 'blahblah';

const usersApi = new Users({ token });

const transformToContributorFormat = ({ data: contributor }) => {
  return {
    login: contributor.username,
    name: contributor.fullName || contributor.username,
    profile: "https://crowdin.com/profile/" + contributor.username,
    avatar_url: contributor.avatarUrl,
    contributions: ["translation"]
  };
}

const getContributors = () => {
  return usersApi.withFetchAll()
    .listProjectMembers(projectId)
    .then(apiResponseWithPaginationInfo => apiResponseWithPaginationInfo.data)
    .then(contributors => contributors.map(transformToContributorFormat))
    .catch(err => console.log(JSON.stringify(err)));
}


const existingContributorsFromFile = getExistingContributors("./.all-contributorsrc");

const isNewContributor = contributor => {
  return !existingContributorsFromFile.find(existingContributor => contributor.profile === existingContributor.profile);
}

const writeToConsole = contributors => {
  console.log(JSON.stringify(contributors, null, 2));
  return contributors;
};

getContributors()
  .then(contributors => contributors.filter(isNewContributor))
  .then(writeToConsole)
  .then(contributors => writeAndroidContributorsResource(contributors, './crowdin_contributors.xml'));
