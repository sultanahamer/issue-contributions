import fs from 'fs';
export const write =(contributors, fileName) => {
  const names = contributors.map(contributor => `<item>${contributor.name}</item>`);
  const urls = contributors.map(contributor => `<item>${contributor.web_url || contributor.profile}</item>`);
  const prepareContentInNewLines = contentArray => contentArray.reduce((content, currentContent) => content + "\n" + currentContent, '');
  const androidContributorsResourceContent = `<?xml version="1.0" encoding="utf-8"?>
    <resources>
        <string-array name="contributor_names">
        ${prepareContentInNewLines(names)}
        </string-array>
        <string-array name="contributor_urls">
        ${prepareContentInNewLines(urls)}
        </string-array>
    </resources>`;
  fs.writeFile(fileName || './contributor.xml', androidContributorsResourceContent, err => {
    if (err) {
      console.error(err)
      return
    }
    console.log('android contributors resource file written successfully');
  });
}
