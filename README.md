# Script to fetch contributors from issues

Contributor on issue is one who has done either of 
1. Opening an issue
2. Comment on an issue

## Setup

1. npm install
2. npx all-contributors init # for the first time

## Running for gitlab

1. npm index.js
2. copy the standard output lines like "npx all-contributors add ..." and paste them on terminal
3. for android app developers, there will be a contributors.xml file with contributors and their gitlab profile url as xml string array resources
4. copy the .all-contributorsrc file to the target repository directory and run `npx all-contributors generate`

## Running for crowdin

1. npm crowdin.js
2. copy the standard output lines and add paste them in .all-contributorsrc file
3. copy contents of contributors.xml to android project's contributors xml resource
